<?php 
class Ermak_Manuscript extends SMC_Post
{ 
	public $metas;
	static $location_manuscript;
	static $location_manuscript_user;
	static function init()
	{
		add_action( 'init',														array(__CLASS__, 'add_manuscript'), 55 );
		add_action( 'smc_add_object_type',										array(__CLASS__, 'smc_add_object_type') , 16);	
		add_action( 'admin_menu',												array(__CLASS__, 'my_extra_fields_event'));	
		add_action( 'save_post_'.ERMAK_MANUSCRIPT_TYPE,							array(__CLASS__, 'save_ermak_Manuscript_data'), 11, 2);
		add_filter( 'manage_edit-'.ERMAK_MANUSCRIPT_TYPE.'_columns',			array(__CLASS__, 'add_views_column'), 4);
		add_filter( 'manage_edit-'.ERMAK_MANUSCRIPT_TYPE.'_sortable_columns',	array(__CLASS__, 'add_views_sortable_column'));
		add_filter( 'manage_'.ERMAK_MANUSCRIPT_TYPE.'_posts_custom_column', 	array(__CLASS__, 'fill_views_column'), 5, 2); // 
	}
	static function get_type()
	{
		return ERMAK_MANUSCRIPT_TYPE;
	}
	
	static function add_manuscript()
	{
		$labels = array(
			'name' => __('Manuscript', ERMAK_MANUSCRIPT),
			'singular_name' => __("Manuscript", ERMAK_MANUSCRIPT), 
			'add_new' => __("add Manuscript", ERMAK_MANUSCRIPT),
			'add_new_item' => __("add Manuscript", ERMAK_MANUSCRIPT), 
			'edit_item' => __("edit Manuscript", ERMAK_MANUSCRIPT),
			'new_item' => __("add Manuscript", ERMAK_MANUSCRIPT),
			'all_items' => __("all Manuscripts", ERMAK_MANUSCRIPT),
			'view_item' => __("view Manuscript", ERMAK_MANUSCRIPT),
			'search_items' => __("search Manuscript", ERMAK_MANUSCRIPT),
			'not_found' =>  __("Manuscript not found", ERMAK_MANUSCRIPT),
			'not_found_in_trash' => __("no found Manuscript in trash", ERMAK_MANUSCRIPT),
			'menu_name' => __("Manuscript", ERMAK_MANUSCRIPT) 
		);
		$args = array(
			 'labels' => $labels
			,'public' => true
			,'show_ui' => true 
			,'has_archive' => true 
			,'exclude_from_search' => true
			,'menu_position' => 7
			,'show_in_menu' => "Ermak_Library_page"
			,'supports' => array(  'title', 'thumbnail', "editor" )
			,'capability_type' => 'post'
		);
		register_post_type(ERMAK_MANUSCRIPT_TYPE, $args);
	}
	static function insert($data)
	{
		$my_post = array(
			'post_title'   		=> $data['title'],
			'post_type' 		=> ERMAK_MANUSCRIPT_TYPE,
			'post_content' 		=> $data['description'] ,
			'post_status'  		=> 'publish',
			'comment_status'	=> 'closed',
		);
		$new_mns				= wp_insert_post( $my_post ); 
		$mns					= static::get_instance( $new_mns );
		$metas					= array(
			"id"				=> $new_mns,
			"owner_id"			=> $data['owner_id'],
			"is_public"			=> $data['is_public'],
			"file_url"			=> $data['file_url'],
			"price"				=> $data['price'],
			"exclusive_price"	=> $data['exclusive_price'],
			"currency_type_id"	=> $data['currency_type_id']
		);
		insertLog("insert", $metas);
		$mns->update_metas($metas);
	}
	function update_metas($meta_array)
	{
		global $wpdb;
		$query						= "SELECT * FROM ".$wpdb->prefix . "ermak_manuscript_meta WHERE id='" . $this->id . "';";
		$m							= $wpdb->get_results($query, ARRAY_A);
		$this->metas				= $m[0];
		require_once(SMC_REAL_PATH.'class/SMC_Object_type.php');
		$SMC_Object_Type 			= new SMC_Object_Type();
		$SMC_Object_Type->init();
		if(!is_array($meta_array))	return;
		$keys						= array();//"id");
		$values						= array();//$this->id);
		foreach($meta_array as $meta=>$val)
		{
			if($SMC_Object_Type->is_meta_exists(static::get_type(), $meta) || $meta=="id")
			{
				//$keys[]				= $meta;
				//$values[]			= "'".$val."'";
				$this->metas[$meta] = $val;
			}
		}
		$keys				= array_merge($keys, 	array_keys($this->metas));
		$values				= array_merge($values, 	array_values($this->metas));
		$vals				= array();
		foreach($values as $val)
			$vals[]			= "'".$val."'";
		$query				= "REPLACE INTO ".$wpdb->prefix . "ermak_manuscript_meta (" . implode(",", $keys) . ") VALUES(".implode(",", $vals). ");";
		//insertLog("update_metas", $query);
		$wpdb->query( $query );	
	}
	public function update_meta($meta, $val)
	{
		global $wpdb;
		require_once(SMC_REAL_PATH.'class/SMC_Object_type.php');
		$SMC_Object_Type 	= new SMC_Object_Type();
		$SMC_Object_Type->init();
		$metas				= array("id");
		$values				= array($this->id);
		if($SMC_Object_Type->is_meta_exists(static::get_type(), $meta))
		{
			$metas[]		= $meta;
			$values[]		= "'".$val."'";
		}		
		$query				= "REPLACE INTO ".$wpdb->prefix . "ermak_manuscript_meta (" . implode(",", $metas) . ") VALUES(".implode(",", $values). ");";
		$wpdb->query( $query );
		$this->metas['$meta']= $val;
	}
	public function get_meta($meta_name)
	{	
		if(!isset($this->metas))
		{	
			global $wpdb;
			$query			= "SELECT * FROM ".$wpdb->prefix . "ermak_manuscript_meta WHERE id='" . $this->id . "';";
			$m				= $wpdb->get_results($query, ARRAY_A);
			$this->metas	= $m[0];
		}
		return $this->metas[ $meta_name ];
	}
	public function get_transfer_form()
	{
		global $Soling_Metagame_Constructor;
		$form				= $Soling_Metagame_Constructor->get_location_selecter(array("id"=>"new_owner_id"));
		$owner_id			= $this->get_meta("owner_id");
		$owner				= SMC_Location::get_instance($owner_id);
		$html				= "
		<div mid='" . $this->id . "' >
			<table>
				<tr>
					<td><center><img style='height:130px;' src='".Ermak_Manuscript_Assistants::get_file_icon($this->get_meta("file_url" ))."'><p>".$this->get("post_title") . "</p></center></td>
					<td><h2>" . 
							__("Transfer Manuscript to other owner", ERMAK_MANUSCRIPT) .
						"</h2>
						<p><label>" . __("Current owner", ERMAK_MANUSCRIPT). "</label><h3>".
							$owner->name.
						"</h3></p>
						<p>
							<label>" . __("Choose new Owner", ERMAK_MANUSCRIPT). "</label><BR>" . 
							$form . 
					"	</p>
						<p>
							<div class='button smc_padding' id='transfer_manuscript'>".__("Transfer", ERMAK_MANUSCRIPT). "</div>
						</p>
					</td>
				</tr>
			</table>
		</div>";
		
		return $html;
	}
	public function get_access_pay_form()
	{
		global $Soling_Metagame_Constructor;
		$currency_type_id	= $this->get_meta("currency_type_id");
		$summae				= $this->get_meta("price");
		$a_price			= SMP_Currency_Type::get_object_price($currency_type_id, $summae);
		$html				= "
		<div mid='" . $this->id . "' >
			<table>
				<tr>
					<td><center><img style='height:130px;' src='".Ermak_Manuscript_Assistants::get_file_icon($this->get_meta("file_url" ))."'><p>".$this->get("post_title") . "</p></center></td>
					<td><h2>" . 
							sprintf(__("Pay access for %s", ERMAK_MANUSCRIPT), "<b>" . $a_price . "</b>") .
						"</h2>
						<p>
							<label>" . __("Currency Account of new Owner", "smp"). "</label><BR>" . 
							SMP_Account::wp_dropdown_all_by_user($currency_type_id, array("name"=>"pmnscr_acc_id", "id"=>"pmnscr_acc_id")) . 
					"	</p>
						<p>
							<div class='button smc_padding' id='access_pay_manuscript'>".__("Pay", "smp"). "</div>
						</p>
					</td>
				</tr>
			</table>
		</div>";
		
		return $html;
	}
	public function get_full_pay_form()
	{
		global $Soling_Metagame_Constructor;
		$currency_type_id	= $this->get_meta("currency_type_id");
		$summae				= $this->get_meta("exclusive_price");
		$a_price			= SMP_Currency_Type::get_object_price($currency_type_id, $summae);
		$html				= "
		<div mid='" . $this->id . "' >
			<table>
				<tr>
					<td><center><img style='height:130px;' src='".Ermak_Manuscript_Assistants::get_file_icon($this->get_meta("file_url" ))."'><p>".$this->get("post_title") . "</p></center></td>
					<td><h2>" . 
							sprintf(__("Pay full for %s", ERMAK_MANUSCRIPT), "<b>" . $a_price . "</b>") .
						"</h2>
						<p>
							<label>" . __("Currency Account of new Owner", "smp"). "</label><BR>" . 
							SMP_Account::wp_dropdown_all_by_user($currency_type_id, array("name"=>"pmnscr_acc_id", "id"=>"pmnscr_acc_id")) . 
					"	</p>
						<p>
							<div class='button smc_padding' id='full_pay_manuscript'>".__("Pay", "smp"). "</div>
						</p>
					</td>
				</tr>
			</table>
		</div>";
		
		return $html;
	}
	//=====================
	
	static function my_extra_fields_event() 
	{
		add_meta_box( 'extra_fields', __('Parameters', "smc"), array(__CLASS__, 'extra_fields_box_func'), ERMAK_MANUSCRIPT_TYPE, 'normal', 'high'  );
	}
	static function extra_fields_box_func($post)
	{
		global $Soling_Metagame_Constructor;
		$manu				= static::get_instance($post);
		$owner_id			= $manu->get_meta( 'owner_id' );
		$file_url 			= $manu->get_meta( 'file_url' );
		$file_url 			= ( false != $file_url ) ? $file_url : '';
		$form				= static::get_admin_form($file_url, $post->post_title);
		$is_public			= $manu->get_meta( 'is_public' );
		
		$download_count		= (int)$manu->get_meta( 'download_count' );
		$exclusive_price	= $manu->get_meta( 'exclusive_price' );
		$price				= $manu->get_meta( 'price' );
		$currency_type_id	= $manu->get_meta( 'currency_type_id' );
		?>
		<div  class="_new-download " style='position:relative; display:inline-block;width:100%;'>
			<div id="upload_form" class="_new-download_cont" style="<?php echo ( !isset( $file_url ) || empty( $file_url ) ) ? 'display: block;' : 'display: none;'; ?>">		
				<div id="ema_upload_modal" class="button _large_button"><?php _e( 'Upload File', ERMAK_MANUSCRIPT ); ?></div>
				<br><input type="hidden" value="<?php echo $file_url; ?>" name="ema_upload_url"/>
			</div>
			<div id="source_form_cont" class="_new-download_cont" style="<?php echo ( !isset( $file_url ) || empty( $file_url ) ) ? 'display: none;' : 'display: block;'; ?>">		
				<div id="source_form"><?php echo $form; ?></div>
				<div id="ema_upload_modal" class="button "><?php _e( 'Upload File', ERMAK_MANUSCRIPT ); ?></div>
				<br><input type="hidden" value="<?php echo $file_url; ?>" name="file_url"/>
			</div>
			<div class="_new-download_cont">		
				<div style='display:inline-block;'>
					<div class="smp_batch_extra_field_column">
						<div class="h" style="display:inline-block;">
							<label  class="h2" for="owner_id"><?php _e("Owner", "smp"); ?></label><br>
							<?php echo $Soling_Metagame_Constructor->get_location_selecter(array("name" => 'owner_id', "class"=>"h7", 'selected' => $owner_id)); ?>
							<p>
								<input  type="checkbox" class="css-checkbox" value="1" name="is_public" id="is_public" <?php checked(1, $is_public);?>/>
								<label class="css-label" for="is_public"><?php _e("Is public", ERMAK_MANUSCRIPT); ?></label>
							</p>
							<p>
								<label for="download_count"><?php _e("Download Count", ERMAK_MANUSCRIPT); ?></label><br>
								<input type="number" min="0" value="<?php echo $download_count;?>" name="download_count" id="download_count" />
							</p>
							<p>
								<label for="price"><?php _e("Access Price", ERMAK_MANUSCRIPT); ?></label><br>
								<input  type="number" value="<?php echo $price; ?>" name="price" id="price" />
							</p>
							<p>
								<label for="exclusive_price"><?php _e("Exclusive Price", ERMAK_MANUSCRIPT); ?></label><br>
								<input  type="number" value="<?php echo $exclusive_price; ?>" name="exclusive_price" id="exclusive_price" />
							</p>
							</p>
								<label for="currency_type_id"><?php _e("Currency Type", "smp"); ?></label><br>
								<?php echo SMP_Currency_Type::wp_dropdown(array("id"=>"currency_type_id", "name"=>"currency_type_id", "selected"=>$currency_type_id)); ?>
							</p>
							<?php echo apply_filters("ermak_manuscript_edit_form", "", $manu->id);	?>
						</div>
					</div>
				</div>
			</div>
		</div>
		<?php 
		wp_nonce_field( basename( __FILE__ ), 'sermak_manuscript_metabox_nonce' );
	}
	static function save_ermak_Manuscript_data ( $post_id, $post) 
	{	
		// проверяем, пришёл ли запрос со страницы с метабоксом
		if ( !isset( $_POST['sermak_manuscript_metabox_nonce'] )
		|| !wp_verify_nonce( $_POST['sermak_manuscript_metabox_nonce'], basename( __FILE__ ) ) )
			return $post_id;
		// проверяем, является ли запрос автосохранением
		if ( defined('DOING_AUTOSAVE') && DOING_AUTOSAVE ) 
			return $post_id;
		// проверяем, права пользователя, может ли он редактировать записи
		if ( !current_user_can( 'edit_post', $post_id ) )
			return $post_id;	
		
		$manu		= static::get_instance($post_id);			
		$manu->update_metas(
			array(
				'id' 				=> $post_id,
				'owner_id' 			=> $_POST['owner_id'],
				'file_url' 			=> $_POST['ema_upload_url'],
				'download_count' 	=> $_POST['download_count'],
				'is_public' 		=> $_POST['is_public'],
				'is_patent' 		=> $_POST['is_patent'],
				'price' 			=> $_POST['price'],
				'exclusive_price' 	=> $_POST['exclusive_price'],
				'currency_type_id' 	=> $_POST['currency_type_id']
			)
		);		
	}
	static function add_views_column( $columns )
	{
		//$columns;
		$posts_columns = array(
			  "cb" 				=> " ",
			  "IDs"	 			=> __("ID", 'smp'),
			  "_image"	 		=> __("Image", 'smp'),
			  "title" 			=> __("Title"),				
			  "owner_id" 		=> __("Owner", "smc"),	
			  "author"			=> __("Author"),
			  "download_count"	=> __("Download Count", ERMAK_MANUSCRIPT),
			  "is_public"		=> __("Is public", ERMAK_MANUSCRIPT),
			  "is_patent"		=> __("Patent", ERMAK_MANUSCRIPT),
			  "price"			=> __("Prices", ERMAK_MANUSCRIPT),
			  //"date"			=> __("Date"),
		   );
		return $posts_columns;			
	}
	// добавляем возможность сортировать колонку
	static function add_views_sortable_column($sortable_columns){
		$sortable_columns['author'] 			= 'author';						
		$sortable_columns['_image'] 			= '_image';						
		$sortable_columns['owner_id'] 			= 'owner_id';						
		$sortable_columns['download_count'] 	= 'download_count';						
		$sortable_columns['is_public'] 			= 'is_public';						
		$sortable_columns['is_patent'] 			= 'is_patent';						
		$sortable_columns['price'] 				= 'price';						
		return $sortable_columns;
	}	
	// заполняем колонку данными	
	static function fill_views_column($column_name, $post_id)
	{
		$manu				= static::get_instance($post_id);
		switch( $column_name) 
		{		
			case 'IDs':
				$color				= $manu->get_meta( "color" );
				echo "<div class='ids'><span>ID</span>".$post_id. "</div>
				<div style='background-color:#$color; width:15px;height:15px;'></div>";
				break;	
			case 'owner_id':	
				$owner_id			= $manu->get_meta("owner_id" );
				$owner				= SMC_Location::get_instance($owner_id);
				echo $owner->name . "<BR><div class='ids'><span>ID</span>".$owner_id. "</div>";
				break;	
			case '_image':					
				echo "<img style='height:60px;' src='".Ermak_Manuscript_Assistants::get_file_icon($manu->get_meta("file_url" ))."'>";
				break;	
			case 'download_count':
				$gt	= (int)$manu->get_meta( 'download_count' );
				echo $gt;
				break;	
			case 'is_public':
				$gt	= $manu->get_meta('is_public' );
				echo $gt ? '<img src="'.SMP_URLPATH.'img/check_checked.png">' : '<img src="'.SMP_URLPATH.'img/check_unchecked.png">';
				break;
			case 'is_patent':
				$gt	= $manu->get_meta('is_patent' );
				echo $gt ? '<img src="'.SMP_URLPATH.'img/check_checked.png">' : '<img src="'.SMP_URLPATH.'img/check_unchecked.png">';
				break;
			case 'price':
				$price				= $manu->get_meta( 'price' );
				$exclusive_price	= $manu->get_meta( 'exclusive_price' );
				$ct					= $manu->get_meta( 'currency_type_id' );
				echo "
				<p>" . __("Access Price", ERMAK_MANUSCRIPT) . ": <b>" . SMP_Currency_Type::get_object_price($ct, $price). "</b></p>
				<p>" . __("Exclusive Price", ERMAK_MANUSCRIPT) . ": <b>" . SMP_Currency_Type::get_object_price($ct, $exclusive_price) . "</b></p>";
				break;
		}
	}
	
	static function get_admin_form($file, $file_name)
	{
		$icon	=  Ermak_Manuscript_Assistants::get_file_icon($file);
		$html	.= "<img src='".$icon."'>
		<p>".$file_name."</p>";
		return $html;
	}
	
	static function is_user_owner($id, $user_id=-1)
	{
		global $Soling_Metagame_Constructor;
		if($user_id==-1)
			$user_id=get_current_user_id();
		$manu	= self::get_instance($id);	
		$owner_loc_id	= $manu->get_meta("owner_id" );
		return $Soling_Metagame_Constructor->user_is_owner($owner_loc_id, $user_id);
	}
	static function get_form($id, $is_owner=-1)
	{		
		global $Soling_Metagame_Constructor;
		$manu				= self::get_instance($id);		
		$is_owner			= $is_owner ==-1 ? $manu->is_mine() : $is_owner;
		$file				= $manu->get_meta("file_url");
		$is_public 			= $manu->get_meta("is_public");
		$download_count 	= $manu->get_meta("download_count");
		$icon				=  Ermak_Manuscript_Assistants::get_file_icon($file);
		$pay				= $is_owner ? __("Download", ERMAK_MANUSCRIPT) : __("Pay Manuscript", ERMAK_MANUSCRIPT);
		$givven_form		= $is_owner ?
		"<div class='little_button mnstrnsfr' id='givven_mnscr'  mid='$id'>".__("Transfer", ERMAK_MANUSCRIPT)."</div>" : "";
		$is_public_form		= $is_owner	? 
		"<input type='checkbox' class='css-checkbox1' " . checked(true, $is_public, 0) . " style='margin:0 0 0  10px;' name='public' id='manuformpublic'/>
		<label class='css-label1' for='manuformpublic'>".__("Is public", ERMAK_MANUSCRIPT). "</label>" : "";
		
		$content			= $manu->get("post_content");
		$content = $is_owner ? "<td><textarea placeholder='" . __("insert description for file", ERMAK_MANUSCRIPT) . "' id='content' style='width: 300px; height:170px;'>".$content."</textarea></td>" :
		"<td style='	max-width: 500px;
						max-height: 200px;
						overflow-x: auto;
						display: inline-block;'>".
				$content.
		"</td>";
		$html				.= "
		<div class='abzaz' style='position:relative; padding-left: 20px!important;' mid='$id'>
			<table >
			<tr>
				<td>
					<img style='height:150px;' src='".$icon."'>
					<div style='position:absolute; top:0; left:0; padding:2px 5px; background:#777; color:white;'>$id</div>
				</td>
				$content
			</tr>"	;
		if($is_public ||  $manu->is_user_may())
		{
			$exec_manuscript_form = "<div mid='$id' class='little_button pay_manuscript'>" . __("Download", ERMAK_MANUSCRIPT) . "</div>";
		}
		if(Ermak_Manuscript_Main::is_finance())
		{
			$ct_id	= $manu->get_meta("currency_type_id");
			$pr		= $manu->get_meta("price");
			$epr	= $manu->get_meta("exclusive_price");
			$price	= SMP_Currency_Type::get_object_price($ct_id, $pr);
			$excpr	= SMP_Currency_Type::get_object_price($ct_id, $epr);
			if($is_owner)
			{
				$price_form		= "<input type='number' class='smc_decor' min='0' id='pr' value='".$pr."'/>";
				$eprice_form	= "<input type='number' class='smc_decor' min='0' id='epr' value='".$epr."'/>";
				$ct_form		= "
				<tr>
					<td> <board_title>" . __("Currency Type", "smp") . "</board_title></td>
					<td><label class='smc_input'>" . SMP_Currency_Type::wp_dropdown(array("id"=>'nm_ct',  'style'=>'width:120px;', "selected"=> $ct_id)) . "</label></td>
				</tr>
				";
				$update_form	= "
				<tr>
					<td> </td>
					<td><div class='updmnscrfrm button smc_padding'>".__("Update")."</div></td>
				</tr>
				";
			}
			else
			{
				$price_form		= $price;
				$eprice_form	= $excpr;
				$ct_form		= "";
				$update_form	= "";
			}
			$html	.= "
				<tr>
					<td>
						<board_title>" . __("Access Price", ERMAK_MANUSCRIPT). "</board_title>
					</td>
					<td>".
						$price_form .
				"	</td>
				</tr>
				<tr>
					<td>
						<board_title>" . __("Exclusive Price", ERMAK_MANUSCRIPT). "</board_title>
					</td>
					<td>".
						$eprice_form.
				"	</td>
				</tr>
				$ct_form 
				
				";	
				$exec_manuscript_form = is_user_logged_in() ? "<div mid='$id' class='little_button pay_manuscript'>" . $pay . "</div>" : "";
		}
		else
		{
			$html	.= "<tr><td> </td></tr>";
		}
		$html		.= "
				<tr>
					<td>
						<board_title>" . __("Download Count", ERMAK_MANUSCRIPT). "</board_title>
					</td>
					<td>".
						$download_count.
					"</td>
				</tr>
				<tr>
					<td>
						<board_title>" . __("Access", ERMAK_MANUSCRIPT). "</board_title>
					</td>
					<td class='exec_manuscript_form'>".
						$exec_manuscript_form . $givven_form . $is_public_form . 
					"</td>
				</tr>
				$update_form
			</table>
		</div>";
		$your_label	= $is_owner	? Assistants::get_your_label(24, 6) : Assistants::get_not_your_label(28, 3);
		return $html. $your_label;
	}
	static function smc_add_object_type($array)
	{
		$ermak_soc						= array( );
		$ermak_soc['t']					= array( 'type'=>'post' );
		$ermak_soc['owner_id']			= array( 'type'=>'id', "object" => SMC_LOCATION_NAME );
		$ermak_soc['currency_type_id']	= array( 'type'=>'id', "object" => SMP_CURRENCY_TYPE );
		$ermak_soc['file_url']			= array( 'type'=>'string' );
		$ermak_soc['download_count']	= array( 'type'=>'number' );
		$ermak_soc['is_public']			= array( 'type'=>'bool' );
		$ermak_soc['price']				= array( 'type'=>'number' );
		$ermak_soc['exclusive_price']	= array( 'type'=>'number' );
		$array[ERMAK_MANUSCRIPT_TYPE]	= $ermak_soc;			
		//		
		return $array;
	}
	static function get_all($metas=-1, $numberposts=-1, $offset=0, $order_by='title', $order='ASC', $is_update=false)
	{
		require_once(SMC_REAL_PATH . "class/SMC_Query.php");
		$SMC_Query = new SMC_Query();	
		$args		= array(
								'orderby'  			=> $order_by,
								'order'  			=> $order,
								'post_status' 		=> 'publish',									
							);
		if(is_array($metas))
		{
			$arr		= array();
			foreach($metas as $key=>$val)
			{
				$ar					= array();
				$ar["value"]		= $val;
				$ar["key"]			= $key;
				
				if(is_array($val))
					$ar["operator"]	= "OR";
				else
					$ar["compare"]	= "=";
				/**/
				$arr[]				= $ar;
			}
			$args['meta_query'][] 	= $arr;			
		}
		//insertLog("get_all", $args);
		static::$all_posts	=  $SMC_Query->get_posts(ERMAK_MANUSCRIPT_TYPE, $args);
		return static::$all_posts;
	}
	function is_mine()
	{
		global $Soling_Metagame_Constructor;
		$owner_id		= $this->get_meta( 'owner_id');
		$owner			= SMC_Location::get_instance($owner_id);
		$owner_type		= $Soling_Metagame_Constructor->get_location_type($owner_id);
		$is_owner		= is_user_logged_in() && $Soling_Metagame_Constructor->cur_user_is_owner($owner_id);
		return $is_owner;
	}
	function do_download()
	{
		global $wpdb;
		$url			= $this->get_meta("file_url");
		$wpdb->query("UPDATE " . $wpdb->prefix . "ermak_manuscript_meta SET download_count=download_count+1 WHERE id='".$this->id."'");
		return $url;
	}
	static function get_location_manuscript()
	{
		if(isset(static::$location_manuscript))
			return static::$location_manuscript;
		global $wpdb;
		static::$location_manuscript	= $wpdb->get_results("SELECT * FROM " . $wpdb->prefix . "ermak_location_manuscript;");
		return static::$location_manuscript;
	}
	
	//проверка доступности манускрипта текущим юзером
	static function get_location_manuscript_user()
	{
		if(!is_user_logged_in())	return array();
		global $wpdb,  $Soling_Metagame_Constructor;
		if(isset(static::$location_manuscript_user))
			return static::$location_manuscript_user;
		$ow_locations_ids					= $Soling_Metagame_Constructor->all_user_locations();
		$query								= "SELECT manuscript_id FROM " . $wpdb->prefix . "ermak_location_manuscript WHERE location_id IN (" . implode(",", $ow_locations_ids) . ") GROUP BY manuscript_id; ";
		static::$location_manuscript_user	= $wpdb->get_col($query);
		//insertLog("get_location_manuscript_user", $res);
		return static::$location_manuscript_user;
	}	
	function is_user_may()
	{
		$res	= static::get_location_manuscript_user();
		return in_array($this->id, $res);
	}
	function add_location_access($location_id)
	{
		global $wpdb;
		$query		= "REPLACE " . $wpdb->prefix . "ermak_location_manuscript VALUES ($location_id," . $this->id . ");";
		//insertLog("add_location_access", $query);
		return $wpdb->query( $query );
	}
	
	function get_table_row($add_row=-1)
	{
		global $Soling_Metagame_Constructor, $SolingMetagameProduction, $SMP_Assistants;
		if(!is_array($add_row))	$add_row = array();
		$id				= $this->id;
		$owner_id		= $this->get_meta( 'owner_id');
		$location		= SMC_Location::get_instance($dis_id);
		$owner			= SMC_Location::get_instance($owner_id);
		$owner_type		= $Soling_Metagame_Constructor->get_location_type($owner_id);
		$is_owner		= is_user_logged_in() && $Soling_Metagame_Constructor->cur_user_is_owner($owner_id);
		$download_count	= $this->get_meta('download_count');
		$file			= $this->get_meta("file_url");
		$pic			= "<img style='height:40px; margin-top:10px;' src='".Ermak_Manuscript_Assistants::get_file_icon($file) ."'/>";
		$ct_id			= $this->get_meta( "currency_type_id" );
		$ct				= SMP_Currency_Type::get_instance( $ct_id );
		$summae			= $this->get_meta("price");
		$summae2		= $this->get_meta("exclusive_price");
		$a_price		= SMP_Currency_Type::get_object_price($ct_id, $summae);
		$e_price		= SMP_Currency_Type::get_object_price($ct_id, $summae2);
		$price			= $this->is_mine() ? 
			"<b>$a_price</b> / <b>$e_price</b>
			<a href='$file' class='mnsdn actual_little_button' mid='$id'><i class='fa fa-download'></i> ".__("Download", ERMAK_MANUSCRIPT)."</a>
			 <div class='mnstrnsfr actual_little_button' mid='$id'><i class='fa fa-sign-out' mid='$id'></i> ".__("Transfer", ERMAK_MANUSCRIPT)."</div>" :
			 
			(static::is_user_may() ?
			"<a href='$file' class='mnsdn actual_little_button' mid='$id'><i class='fa fa-download'></i> ".__("Download", ERMAK_MANUSCRIPT)."</a>" :
			//static::is_user_may(true) :
			"<div class='little_button pay_access_manuscript' mid='$id'>".
				sprintf(__("Pay access for %s", ERMAK_MANUSCRIPT), $a_price).
			"</div>").
			"<div class='little_button pay_manuscript' mid='$id'>".
				sprintf(__("Pay full for %s", ERMAK_MANUSCRIPT), $e_price).
			"</div>";
		
		//$your			= $is_owner ? '<div style="height:20px;"></div><div class=smc-your-label style="bottom:0; right:-10px;">'.__("It's your", "smc").'</div>' : "";
		$bg				= $is_owner	? " style='background:rgba(0,0,0,0.25)' " : "";		
		$your_label		= $is_owner	? Assistants::get_short_your_label(30, array(1, 1)) : "";
		$add_titles		= "";
		$add_contents	= "";
		foreach($add_row as $row)
		{
			$add_titles		.= "<td class='descr' $bg>" . $row['title'] . "</td>";
			$add_contents	.= "<td $bg>" . $row['content'] . $your_label . "</td>";
		}
		if($SolingMetagameProduction->is_finance())
		{
			$pay_button;
			$pay_button 	= $price;
			switch(true)
			{
				case $download_count:
					//break;
				default:
					//$pay_button	= "";
					break;
			}
			$price_text	= "
			<td width='200' $bg>".
				$pay_button. 
			"</td>";
			$price_th	= "
			<td class='descr' $bg>".
				(__("Access", ERMAK_MANUSCRIPT)).
			"</td>
			";
			if(!is_user_logged_in())
			{
				$price_text	= $price_th	= "";
			}
		}
		$cont_content	= "
			<td width='180' $bg class='manu_table_descr'>" .
				apply_filters("ermak_manuscript_table_row_description", wp_trim_words($this->get("post_content"), 4, " ..."), $id).
			"</td>";
		$cont_th	= "
			<td class='descr' $bg>".
				__("Description", ERMAK_MANUSCRIPT).
			"</td>";
		$html			= "
		<tr>
			<td rowspan='2' width='70' $bg>
				<center>
					<div class='button show_mnsdn' mid='$id'>".
						$pic.
						"<p class='lp-batch-location_type' style='width:75px; color:#FFF;'>".$this->get("post_title")."</p>$your_label
					</div> 
				</center>
			</td>
			$cont_th
			<td class='descr' width='300'  $bg>".
				__("Owner", "smp").
			"</td>".
			$price_th . $add_titles . //"<td $bg></td>".
		"</tr>
		<tr>".
			$cont_content.
			"<td width='180' $bg>".
				apply_filters("e_manuscript_form_owner","
					
						<span class='lp-batch-location_type'>".
							$owner_type->post_title.
						'</span>
						<span>'. 
							$owner->name.
						'</span>
					', 
				$id ).
			"</td>".
			apply_filters("e_manuscript_form_price", $price_text, $id ). 
			apply_filters("e_manuscript_add_characteristics", "", $id ). 
			$add_contents.
			//$button.
		"</tr>
		<tr>
			<td colspan='19' class='tbhr' $bg></td>
		</tr>";		
		
		return $html;
	}
} 
?>